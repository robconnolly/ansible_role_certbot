Ansible Certbot Role
=========

An Ansible role to certbot and create certificates via nginx.

Requirements
------------

None

Role Variables
--------------

The following variables are available:

* `certbot_email` - email address for certificate reminders
* `certbot_certs` - list of domains to request certs for

Dependencies
------------

None.

Example Playbook
----------------

User the role as follows:

```yaml
  - hosts: servers
    vars:
      certbot_email: "me@example.com"
      certbot_certs:
        - test1.example.com
        - test2.example.com
    roles:
      - role: certbot
```

License
-------

AGPLv3

Author Information
------------------

Rob Connolly [https://webworxshop.com](https://webworxshop.com)
